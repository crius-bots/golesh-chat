package goleshchat

const querySubscribeToMessages = `subscription($channel: ID) {
	chatMessage(channelId: $channel) {
		user {
			username
			id
		}
		message
		id
		insertedAt
		channel {
			id
			status
			streamer {
				id
				username
			}
		} 
	} 
}`

const querySendMessage = `mutation($channel: ID, $message: String) {
	createChatMessage(channelId:$channel, message: {message: $message}) {
		message
	}
}
`
