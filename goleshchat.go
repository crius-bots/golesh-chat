package goleshchat

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"strings"
	"time"
)

const (
	glimeshSocketBase = "wss://glimesh.tv/api/socket/websocket"
	glimeshAPIVersion = "2.0.0"
)

type _GQLContainer struct {
	Query     string      `json:"query"`
	Variables interface{} `json:"variables"`
}

var replacer = strings.NewReplacer("\n", " ", "\t", "")

type OnMessage func(*Session, *ChatMessage)

// Make a new client.
// this code doesn't handle getting the token, sorry.
func New(token string, onMessage OnMessage) *Session {
	if onMessage == nil {
		onMessage = func(_ *Session, _ *ChatMessage) {}
	}

	return &Session{
		token:     token,
		writeChan: make(chan interface{}),
		onMessage: onMessage,
		stop:      make(chan bool),
	}
}

type Session struct {
	wsConn    *websocket.Conn
	token     string
	writeChan chan interface{}
	stop      chan bool
	onMessage OnMessage
}

func (s *Session) SetOnMessageHandler(h OnMessage) {
	s.onMessage = h
}

func (s *Session) SendMessage(message string, channelID string) {
	payload := &_GQLContainer{
		Query: replacer.Replace(querySendMessage),
		Variables: map[string]interface{}{
			"channel": channelID,
			"message": message,
		},
	}

	connectionMessage := newPhoenixTuple("__absinthe__:control", "doc", payload).ToSlice()

	s.writeChan <- connectionMessage
}

func (s *Session) Open() error {
	dialer := &websocket.Dialer{}

	conn, _, err := dialer.Dial(fmt.Sprintf("%s?vsn=%s&token=%s", glimeshSocketBase, glimeshAPIVersion, s.token), nil)
	if err != nil {
		return err
	}
	s.wsConn = conn

	// we have to send the first message
	joinMsg := newPhoenixTuple("__absinthe__:control", "phx_join", nil).ToSlice()

	err = conn.WriteJSON(joinMsg)
	if err != nil {
		return err
	}

	var data []json.RawMessage

	// read the first resp
	err = conn.ReadJSON(&data)
	if err != nil {
		return err
	}

	readyRes := &phoenixTuple{}
	readyRes.FromSlice(data)

	// before heartbeats, create the writer - it goes from a channel to the websocket
	go func() {
		for {
			select {
			case data := <-s.writeChan:
				{
					err := conn.WriteJSON(data)
					if err != nil {
						log.Fatal(err)
					}
				}
			case <-s.stop:
				return
			}
		}

		s.wsConn.Close()
	}()

	// now start up the heartbeats
	go s.doHeartbeats()

	// this is the reader
	go s.reader()

	return nil
}

func (s *Session) ConnectToChannel(channelID int) {
	payload := &_GQLContainer{
		Query: replacer.Replace(querySubscribeToMessages),
		Variables: map[string]interface{}{
			"channel": channelID,
		},
	}

	connectionMessage := newPhoenixTuple("__absinthe__:control", "doc", payload).ToSlice()

	s.writeChan <- connectionMessage
}

func (s *Session) Close() {
	s.stop <- true
	close(s.writeChan)
}

func (s *Session) reader() {
	for {
		select {
		case <-s.stop:
			return
		default:
		}

		_, msg, err := s.wsConn.ReadMessage()
		if err != nil {
			log.Fatal(err)
		}

		var data []json.RawMessage

		json.Unmarshal(msg, &data)

		message := &phoenixTuple{}
		message.FromSlice(data)

		if message.Event == "subscription:data" {
			// we've got something about a channel we have a sub to!

			data := &struct {
				Result struct {
					Data struct {
						ChatMessage *ChatMessage
					}
				}
			}{}

			err := json.Unmarshal(message.Payload.(json.RawMessage), data)
			if err != nil {
				panic(err)
			}

			s.onMessage(s, data.Result.Data.ChatMessage)
		}
	}
}

func (s *Session) doHeartbeats() {
	ticker := time.NewTicker(time.Second * 28)
	defer ticker.Stop()

	heartbeat := newPhoenixTuple("phoenix", "heartbeat", nil).ToSlice()

	for {
		select {
		case <-ticker.C:
			s.writeChan <- heartbeat
		case <-s.stop:
			return
		}
	}
}
