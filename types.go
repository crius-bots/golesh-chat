package goleshchat

import "time"

type User struct {
	Username string
	ID       string
}

type ChatMessage struct {
	User             *User
	Message          string
	ID               string
	InsertedAtString string `json:"insertedAt"`
	Channel          *Channel
}

func (m *ChatMessage) InsertedAt() (*time.Time, error) {
	t, err := time.Parse("2006-01-02T15:04:05", m.InsertedAtString)
	if err != nil {
		return nil, err
	}

	return &t, nil
}

type Channel struct {
	Streamer *User
	ID       string
}
